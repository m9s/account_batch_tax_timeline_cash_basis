# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pyson import And, Equal, Eval, Bool, If, PYSONEncoder
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    'Account Batch Entry Line'
    _name = 'account.batch.line'

    def __init__(self):
        super(Line, self).__init__()
        # Basic concept of cash basis in batch:
        # To be able to match the correct taxes to use, we need an invoice
        # creating the necessary receivable moves with cash basis taxes.
        # So we require invoices for receivable moves.
        self.invoice = copy.copy(self.invoice)
        if 'readonly' in self.invoice.states:
            if (PYSONEncoder().encode(self.invoice.states['readonly'])
                    == PYSONEncoder().encode(Bool(Eval('tax')))):
                self.invoice.states['readonly'] = False
        ## TODO: if account kind == receivable -> invoice required
        ## make this work
        self.invoice.states['required'] = And(
            Bool(Equal(Eval('contra_account.kind'), 'receivable')),
            Bool(Equal(Eval('contra_account.fiscalyear.taxation_method'),
                    'cash_income')))

        ## TODO: Field "fiscalyear" doesn\'t exist on "account.tax"
        ## This tax domain depending on taxation_method is difficult to implement
        # tax
        self.tax = copy.copy(self.tax)
        #tax_domain = [
        #    ('fiscalyear.taxation_method', '=', 'cash_income'),
        #    ('payment_tax', If(Bool(Eval('invoice')), '=', '!='), True),
        #    ]
        #if self.tax.domain is None:
        #    self.tax.domain = tax_domain
        #else:
        #    self.tax.domain += tax_domain
        if 'readonly' in self.tax.states:
            if (PYSONEncoder().encode(self.tax.states['readonly'])
                    == PYSONEncoder().encode(Bool(Eval('invoice')))):
                self.tax.states['readonly'] = False
        if 'invoice' not in self.tax.depends:
            self.tax.depends = copy.copy(self.tax.depends)
            self.tax.depends.append('invoice')

        # invoice
        if 'date' not in self.invoice.depends:
            self.invoice.depends = copy.copy(self.invoice.depends)
            self.invoice.depends.append('date')
        if 'invoice' not in self.invoice.on_change:
            self.invoice.on_change = copy.copy(
                self.invoice.on_change)
            self.invoice.on_change.append('invoice')
        if 'date' not in self.invoice.on_change:
            self.invoice.on_change = copy.copy(
                self.invoice.on_change)
            self.invoice.on_change.append('date')

        # contra_account
        if 'invoice' not in self.contra_account.depends:
            self.contra_account.depends = copy.copy(
                self.contra_account.depends)
            self.contra_account.depends.append('invoice')
        if 'invoice' not in self.contra_account.on_change:
            self.contra_account.on_change = copy.copy(
                self.contra_account.on_change)
            self.contra_account.on_change.append('invoice')
        if 'date' not in self.contra_account.on_change:
            self.contra_account.on_change = copy.copy(
                self.contra_account.on_change)
            self.contra_account.on_change.append('date')

        self._reset_columns()

    def on_change_contra_account(self, vals):
        res = super(Line, self).on_change_contra_account(vals)
        res['tax'] = self._get_invoice_due_tax(vals)
        return res

    def on_change_invoice(self, vals):
        res = super(Line, self).on_change_invoice(vals)
        res['tax'] = self._get_invoice_due_tax(vals)
        return res

    def _get_invoice_due_tax(self, vals):
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        invoice_tax_obj = pool.get('account.invoice.tax')

        res = False
        fiscalyear = fiscalyear_obj.get_fiscalyear(vals.get('date'))
        if fiscalyear.taxation_method == 'cash_income':
            invoice_id = vals.get('invoice')
            if invoice_id:
                invoice_tax_id = invoice_tax_obj.search([
                        ('invoice', '=', invoice_id)])
                invoice_tax = invoice_tax_obj.browse(invoice_tax_id)[0]
                res = invoice_tax.tax.due_tax.id
        return res

    def hook_tax_set_debit_credit(self, batch_line):
        '''
        This hook excludes the setting of a debit or credit amount for payment
        taxes.
        '''
        pool = Pool()
        tax_obj = pool.get('account.tax')

        res = super(Line, self).hook_tax_set_debit_credit(batch_line)
        if batch_line.get('tax'):
            tax = tax_obj.browse(batch_line['tax'])
            res = not tax.payment_tax
        return res

Line()


class MoveLine(ModelSQL, ModelView):
    _name = 'account.move.line'

    def _check_amount_line(self, line):
        # Don't check receivable lines. The sum of their tax lines doesn't match
        # the amount of the move line when using payment taxes for cash_income.
        if (line.period.fiscalyear.taxation_method == 'cash_income' and
                line.account.kind == 'receivable'):
            return True
        return super(MoveLine, self)._check_amount_line(line)

MoveLine()
