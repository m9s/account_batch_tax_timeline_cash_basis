# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Tax Timeline Cash Basis',
    'name_de_DE': 'Buchhaltung Stapelbuchung Steuer Gültigkeitsdauer Ist-Versteuerung',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Cash Basis Taxation for Batch Entries
    ''',
    'description_de_DE': '''Ist-Versteuerung für die Stapelbuchung
    ''',
    'depends': [
        'account_batch_tax_timeline',
        'account_timeline_invoice_cash_basis',
    ],
    'xml': [
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
